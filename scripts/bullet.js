import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { Impact } from './impact.js';
import { GameObject } from './gameObject.js';
import * as IMPACTTYPE from './impactType.js';

class Bullet extends GameObject {

    origin;
    direction;

    speed;    
    lifetime;
    object;
    sound;
    destroyed;

    _raycaster;
    _raycasterDebugArrow;
    _impactSounds;
    _fleshImpactSounds;

    constructor( origin, direction ) {
        
        super();
        this.direction = direction;
        this.origin = origin;

        this.speed = 100;
        this.lifetime = 1000; //game ticks
        this.destroyed = false;

        let impactSound1 = GAME.Globals.assets.find(x => x.name === 'impact_1');
        let impactSound2 = GAME.Globals.assets.find(x => x.name === 'impact_2');
        let impactSound3 = GAME.Globals.assets.find(x => x.name === 'impact_3');
        let impactSound4 = GAME.Globals.assets.find(x => x.name === 'impact_4');

        let fleshImpactSound1 = GAME.Globals.assets.find(x => x.name === 'flesh_impact_1');
        let fleshImpactSound2 = GAME.Globals.assets.find(x => x.name === 'flesh_impact_2');
        let fleshImpactSound3 = GAME.Globals.assets.find(x => x.name === 'flesh_impact_3');

        this._impactSounds = [impactSound1, impactSound2, impactSound3, impactSound4];
        this._fleshImpactSounds = [fleshImpactSound1, fleshImpactSound2, fleshImpactSound3];

        this.sound = new THREE.PositionalAudio( GAME.getListener() );
    }

    destroy() {
        GAME.Globals.scene.remove( this.object );
        this.object.geometry.dispose();
        this.object.material.dispose();
        if (GAME.Globals.debug) {
            GAME.Globals.scene.remove( this._raycasterDebugArrow );
        }
    }   

    initialize() {
        let geometry = new THREE.BoxGeometry( 0.1, 0.1, 1.5 );
        let material = new THREE.MeshStandardMaterial({emissive: 0xF7CB77});
    
        this.object = new THREE.Mesh( geometry, material );
        this.object.position.set(this.origin.x, this.origin.y, this.origin.z);
        this.object.quaternion.set(this.direction.x, this.direction.y, this.direction.z, this.direction.w);

        GAME.Globals.scene.add( this.object );

        // let raycastDirectionVector = new THREE.Vector3( 0, 0, -1 );
        // raycastDirectionVector.applyQuaternion( this.object.quaternion );
        // this._raycaster = new THREE.Raycaster( new THREE.Vector3(), raycastDirectionVector, 0, this.speed );
        // if (GAME.Globals.debug) {
        //     this._raycasterDebugArrow = new THREE.ArrowHelper( this._raycaster.ray.direction, this._raycaster.ray.origin, 100, Math.random() * 0xffffff );
        //     GAME.Globals.scene.add(this._raycasterDebugArrow);
        // }
   
        super.initialize();
        let physicsShape = new Ammo.btBoxShape( new Ammo.btVector3( 0.1, 0.1, 1.5 ) );
        const mass = 0.1;
        this.initializePhysics(physicsShape, mass);
        this.physicsObject.setLinearVelocity(new Ammo.btVector3(0, 0, this.speed));
    }

    onImpact(impactType) {
        this.destroyed = true;
        this.object.visible = false;
        let impact = new Impact( this.object.position, impactType );
        let soundindex = 0;
        switch (impactType) {
            case IMPACTTYPE.fleshImpactType:  
                impact.initialize();
                soundindex = HELPER.getRandomInt(0, this._fleshImpactSounds.length - 1);
                this.sound.setBuffer( this._fleshImpactSounds[soundindex].contents );
                this.sound.setVolume( 1.5 );
                this.sound.setRefDistance( 10 );
                break;
            default:
                impact.initialize();
                soundindex = HELPER.getRandomInt(0, this._impactSounds.length - 1);
                this.sound.setBuffer( this._impactSounds[soundindex].contents );
                this.sound.setVolume( 0.05 );
                this.sound.setRefDistance( 10 );  
        }
        this.sound.onEnded = () => {
            this.destroy();
            this.sound.isPlaying = false;
        };
        this.sound.play();
        this.object.add(this.sound);
    }

    checkIntersects() {
        var intersects = this._raycaster.intersectObjects( GAME.Globals.solidObjects, true);
        if (intersects.length > 0) {
            for (let i = 0; i < intersects.length; i++) {
                if (intersects[i].object.userData.onShot) {
                    return intersects[i].object.userData.onShot();
                }
            }
            return IMPACTTYPE.defaultImpactType;
        } else {
            return false;
        }
    }

    //this should be onAnimate instead
    onGameTick() {
        // if (!this.destroyed) {
        //     if (this.lifetime < 1) {
        //         this.destroy();
        //         return;
        //     }
        //     this.lifetime--;

        //     this._raycaster.ray.origin.copy( this.object.position );

        //     if (GAME.Globals.debug) { 
        //         this._raycasterDebugArrow.position.copy( this.object.position );
        //     }

        //     let impactType = this.checkIntersects()
        //     if (impactType) {
        //         this.onImpact(impactType);
        //     } else {
        //         this.object.translateZ( -this.speed );
        //     }

        // }
    }

};

export { Bullet };