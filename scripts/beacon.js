import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Beacon extends GameObject {

    asset;
    object;
    position;
    light;
    height;

    constructor() {
        super();
        this.asset = GAME.Globals.assets.find(x => x.name === 'beacon');
        this.object = this.asset.contents.clone();
        this.object.scale.set(3, 3, 3);
    }

    initialize( position ) {

        this.object.position.copy( position );

        this.light = new THREE.PointLight( 0xFF0A00, 0.5, 30 );
        
        this.object.children.forEach(child => {
            if (child.name === "Bulb") {
                child.add(this.light);
            }
            GAME.Globals.solidObjects.push( child );
        });
   
        super.initialize();
    }
    

};

export { Beacon };