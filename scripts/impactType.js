class ImpactType {

    name;

    constructor( name ) {
        this.name = name;
    }

};

let defaultImpactType = new ImpactType("default");
let fleshImpactType = new ImpactType("flesh");

export { ImpactType, defaultImpactType, fleshImpactType };