import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { GameObject } from './gameObject.js';
import * as IMPACTTYPE from './impactType.js';

class Worm extends GameObject {

    asset;
    object;
    object;
    direction;
    velocity;
    prevTime = performance.now();
    speed = 0.1;
    rotationSpeed = 0.01;
    size = 8;
    health = 50;
    mass = 50;
    sinkDepth = 1;
    target;
    isDead;
    isStartingJump;
    isJumping;
    isUnderground;
    sound;
    
    _prevTime = performance.now();
    _lastDirection;
    _rotationDirection;

    _directionDebugArrow;
    _downDebugArrow;
    _raycaster;
    _downRaycaster;

    _sounds;
    _deathSound;

    constructor() {

        super();

        this.asset = GAME.Globals.assets.find(x => x.name === 'worm');
        this.object = this.asset.contents.clone();
        this.object.scale.set(this.size, this.size, this.size);

        let sound_1 = GAME.Globals.assets.find(x => x.name === 'spider_sound_1');
        let sound_2 = GAME.Globals.assets.find(x => x.name === 'spider_sound_2');
        let sound_3 = GAME.Globals.assets.find(x => x.name === 'spider_sound_3');
        let sound_4 = GAME.Globals.assets.find(x => x.name === 'spider_sound_4');
        this._deathSound = GAME.Globals.assets.find(x => x.name === 'spider_death');

        this._sounds = [sound_1, sound_2, sound_3, sound_4];

        this.sound = new THREE.PositionalAudio( GAME.getListener() );

        this._lastDirection = new THREE.Quaternion();
        this._lastDirection.copy(this.object.quaternion);
        this._rotationDirection = new THREE.Vector3();
 
    }

    async initialize( position ) {

        this.object.position.copy(position);
        this.object.add(this.sound);

        this.target = new THREE.Vector3(0, 0, 0);
        this.direction = new THREE.Vector3(0, 0, 1);
        this.velocity = new THREE.Vector3();

        if (GAME.Globals.debug) {
            this._directionDebugArrow = new THREE.ArrowHelper( this.direction, this.object.position, 10, Math.random() * 0xffffff );
            GAME.Globals.scene.add(this._directionDebugArrow);

            this._downDebugArrow = new THREE.ArrowHelper( new THREE.Vector3( 0, -1, 0 ), this.object.position, 10, Math.random() * 0xffffff );
            GAME.Globals.scene.add(this._downDebugArrow);
        }

        this._raycaster = new THREE.Raycaster( new THREE.Vector3(), this.direction, 0, this.speed );
        this._downRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3( 0, -1, 0 ), 0, 10 );

        this.isDead = false;

        this.object.children.forEach(child => {
            child.userData.onShot = this.onShot.bind(this);
            GAME.Globals.solidObjects.push(child);
        });
              
        super.initialize();
    }

    

    die() {
        this.isDead = true;
        this.onGameTick = () => {}; //performance(?)
        this.onAnimate = () => {};    

        this.object.children.forEach(child => {
            const index = GAME.Globals.solidObjects.indexOf(child);
            GAME.Globals.solidObjects.splice(index, 1);
        });

        if (GAME.Globals.debug) {
            GAME.Globals.scene.remove( this._directionDebugArrow );
            GAME.Globals.scene.remove( this._downDebugArrow );
        }

        this.sound.setBuffer( this._deathSound.contents );
        this.sound.setVolume( 2 );
        this.sound.setRefDistance( 10 );
        if (this.sound.isPlaying) {
            this.sound.stop();
        }
        this.sound.play();
    }

    onShot() {
        console.log((HELPER.getRandomBool() ? 'oof' : 'ouch') + (HELPER.getRandomBool() ? ' owie' : ''), this.health);
        if (this.health > 1) {
            this.health--;
        } else {
            this.die();
        }
        return IMPACTTYPE.fleshImpactType;
    }

    jump() {
        //yeet
        this.isJumping = true;
        this.velocity.y = 140;

        let soundindex = HELPER.getRandomInt(0, this._sounds.length - 1);
        this.sound.setBuffer( this._sounds[soundindex].contents );
        this.sound.setVolume( 2 );
        this.sound.setRefDistance( 10 );
        if (this.sound.isPlaying) {
            this.sound.stop();
        }
        this.sound.play();
    }

    setDirectionToTarget() {
        this.object.lookAt(this.target.x, this.object.position.y, this.target.z);
        this._lastDirection.slerp(this.object.quaternion, this.rotationSpeed);

        this.direction = new THREE.Vector3(0, 0, 1);
        this.direction.applyQuaternion( this._lastDirection );

        this._rotationDirection.set(0, 0, 0);
        this._rotationDirection.addVectors(this.direction, this.object.position);
        this.object.lookAt(this._rotationDirection.x, this.object.position.y, this._rotationDirection.z);
        
    }

    onGameTickAlways() {
        var time = performance.now();

        if (!GAME.Globals.paused && !this.isDead) {

            if (GAME.Globals.debug) {
                this._directionDebugArrow.position.copy( this.object.position );
                this._directionDebugArrow.setDirection( this.direction );
                this._downDebugArrow.position.copy( this.object.position );
            }

            this.target = GAME.Globals.player.object.position; //detect the player first?

            if (this.isJumping) {
                console.log('hup!');
                //jumping
                let delta = ( time - this.prevTime ) / 1000;

                this._raycaster.ray.origin.copy( this.object.position );
                this._raycaster.ray.direction.copy( this.direction );
                let intersects = this._raycaster.intersectObjects( GAME.Globals.solidObjects, true);
                if (intersects.length === 0) {
                    this.object.translateZ( this.speed * 2 );
                }

                this._downRaycaster.ray.origin.copy( this.object.position );
                let downwardIntersection = this._downRaycaster.intersectObject( GAME.Globals.currentRegion.object );
                if (downwardIntersection.length && downwardIntersection[0].distance < 0.5) {
                    this.object.position.y = downwardIntersection[0].point.y - this.sinkDepth;
                    this.isJumping = false;
                }

                let n = new THREE.Vector3(this.object.position.x, this.object.position.y + (this.velocity.y * delta), this.object.position.z);
                this.object.rotation.x += 1;
                this.object.position.y = n.y;
                this.velocity.y -= GAME.Globals.gravity * this.mass * delta;

                //this.object.lookAt(this.object.position);

            } else if (this.target !== null) {
                //walking
                this.setDirectionToTarget();
    
                this._raycaster.ray.origin.copy( this.object.position );
                this._raycaster.ray.direction.copy( this.direction );
    
                let intersects = this._raycaster.intersectObjects( GAME.Globals.solidObjects, true);
                if (intersects.length === 0) {
                    this.object.position.add(this.direction.multiplyScalar(this.speed));
                }
    
                this._downRaycaster.ray.origin.copy( this.object.position );
                let downwardIntersections = this._downRaycaster.intersectObject( GAME.Globals.currentRegion.object );
                if (downwardIntersections.length) {
                    this.object.position.y = downwardIntersections[0].point.y - this.sinkDepth;
                }
                let worldPosition = new THREE.Vector3();
                this.object.getWorldPosition ( worldPosition )
                if (worldPosition.distanceTo(this.target) < 30) {
                    this.jump();
                }
            }

            if (this.object.position.y < (-this.size/2 - this.sinkDepth)) {
                this.object.position.y = -this.size/2 - this.sinkDepth;
                this.isUnderground = true;
                this.isJumping = false;
            }
        }
        this.prevTime = time;
    }

};

export { Worm };