import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';

class Skybox {

    constructor() {
        this.cubeTexture = GAME.Globals.assets.find(x => x.name === 'skybox');
    }

    initialize() {
        let texture = this.cubeTexture.contents;
        GAME.Globals.scene.background = texture;        
    }

    // initialize() {
    //     let texture = this.textureAsset.contents;
    //     const shader = THREE.ShaderLib.equirect;
    //     const material = new THREE.ShaderMaterial({
    //         fragmentShader: shader.fragmentShader,
    //         vertexShader: shader.vertexShader,
    //         uniforms: shader.uniforms,
    //         depthWrite: false,
    //         side: THREE.BackSide,
    //         fog: false,
    //     });
    //     material.uniforms.tEquirect.value = texture;

    //     const plane = new THREE.BoxBufferGeometry(2000, 2000, 2000);            
    //     let skymesh = new THREE.Mesh(plane, material);
    //     GAME.Globals.scene.add(skymesh);
    // }

};

export { Skybox };