import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Door extends GameObject {

    object;
    pivot;
    locked;
    _state; //"opening", "closing", "open", "closed"

    constructor(pivot) {
        super();
        this.pivot = pivot;
        this.object = this.pivot.children[0];
        this._state = "closed";
    }

    onInteract() {
        if (!this.locked) {
            if (this._state === "open") {
                this._state = "closing";
            } else if (this._state === "closed") {
                this._state = "opening";
            }
        }
    }

    initialize( position ) {

        this.object.userData.onInteract = this.onInteract.bind(this);
        GAME.Globals.interactableObjects.push( this.object );
        GAME.Globals.solidObjects.push( this.object );
   
        super.initialize();
    }

    lock() {
        this.locked = true;
        this._state = "closed";
        this.pivot.rotation.y = 0;
    }

    unlock() {
        this.locked = false;
    }

    onGameTick() {
        switch(this._state) {
            case "opening":
                if (this.pivot.rotation.y < Math.PI/1.5) {
                    this.pivot.rotation.y += 0.02;
                } else {
                    this._state = "open";
                    this.pivot.rotation.y = Math.PI/1.5;
                }                
                break;
            case "closing":
                if (this.pivot.rotation.y > 0) {
                    this.pivot.rotation.y -= 0.02;
                } else {
                    this._state = "closed";
                    this.pivot.rotation.y = 0;
                } 
                break;
        }
    }
    

};

export { Door };