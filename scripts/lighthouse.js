import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Lighthouse extends GameObject {

    asset;
    object;
    position;

    light;

    constructor() {
        super();
        this.asset = GAME.Globals.assets.find(x => x.name === 'lighthouse');
        this.object = this.asset.contents.clone();
        this.object.scale.set(6, 6, 6);
    }

    initialize( position ) {

        this.object.position.copy( position );

        this.light = new THREE.PointLight( 0xEDB68D, 2, 50 );

        GAME.Globals.scene.add( this.object );
        this.object.children.forEach(child => {
            if (child.name === "Light") {
                child.add(this.light);
            }
            GAME.Globals.solidObjects.push( child );
        });
   
        super.initialize();
    }
    

};

export { Lighthouse };