import * as GAME from './game.js';
import * as HELPER from './helper.js';

class GameObject {

    uuid;
    object;
    physicsObject;
    physical;

    constructor( name ) {
        this.uuid = HELPER.getUUID();
    }

    initialize() {
        GAME.Globals.gameObjects.set(this.uuid, this);
        this.physical = false;
    }

    initializePhysics(physicsShape, mass) {
        if (!physicsShape || !this.object) {
            console.error("Attempted to initialize physics on an undefined object");
            return false;
        }
        this.physical = true;
        const margin = 0.05;
        physicsShape.setMargin( margin );
        let localInertia = new Ammo.btVector3( 0, 0, 0 );
        physicsShape.calculateLocalInertia( mass, localInertia );
        let transform = new Ammo.btTransform();
        transform.setIdentity();
        let pos = this.object.position;
        transform.setOrigin( new Ammo.btVector3( pos.x, pos.y, pos.z ) );
        let motionState = new Ammo.btDefaultMotionState( transform );
        let rbInfo = new Ammo.btRigidBodyConstructionInfo( mass, motionState, physicsShape, localInertia );
        let body = new Ammo.btRigidBody( rbInfo );

        this.object.userData.physicsBody = body;
        this.physicsObject = body;
        
        GAME.Globals.physicsObjects.push(this.object);
        GAME.Globals.Ammo.physicsWorld.addRigidBody(body);    
    }

    onGameTick() {

    }

    onGameTickAlways() {
        if (GAME.Globals.paused === false) {
            this.onGameTick();
        }
    }

};

export { GameObject };