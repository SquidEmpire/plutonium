import * as THREE from './vendor/three.module.js';
import * as HELPER from './helper.js';
import * as GAME from './game.js';
import { Bullet } from './bullet.js';

class Tommygun2 {

    sprite;
    pivot;
    origin;
    isShooting;
    rateOfFire; //bullets per second
    sound;

    originObject;

    //animation internals
    _restingPosition;
    _shootInterval;
    _animateFrameTimeout;
    _mgsounds;

    //sprites
    currentSprite;
    neutralSprite;
    shootingSprite1;
    shootingSprite2;
    altShootingSprite1;
    altShootingSprite2;

    constructor(  ) {
        this.isShooting = false;
        this.rateOfFire = 10;
        this._restingPosition = new THREE.Vector3(1, -1.9, -3.5);
        this.neutralTextureAsset = GAME.Globals.assets.find(x => x.name === 'tommy_fire_neutral');
        this.fire1TextureAsset = GAME.Globals.assets.find(x => x.name === 'tommy_fire_1');
        this.fire2TextureAsset = GAME.Globals.assets.find(x => x.name === 'tommy_fire_2');
        this.fireAlt1TextureAsset = GAME.Globals.assets.find(x => x.name === 'tommy_fire_alt_1');
        this.fireAlt2TextureAsset = GAME.Globals.assets.find(x => x.name === 'tommy_fire_alt_2');

        let mg1 = GAME.Globals.assets.find(x => x.name === 'mg1');
        let mg2 = GAME.Globals.assets.find(x => x.name === 'mg2');
        let mg3 = GAME.Globals.assets.find(x => x.name === 'mg3');
        let mg4 = GAME.Globals.assets.find(x => x.name === 'mg4');
        let mg5 = GAME.Globals.assets.find(x => x.name === 'mg5');
        let mg6 = GAME.Globals.assets.find(x => x.name === 'mg6');
        let mg7 = GAME.Globals.assets.find(x => x.name === 'mg7');
        let mg8 = GAME.Globals.assets.find(x => x.name === 'mg8');
        this._mgsounds = [mg1, mg2, mg3, mg4, mg5, mg6, mg7, mg8];

        this.sound = new THREE.PositionalAudio( GAME.getListener() );

    }

    initialize() {
        this.neutralSprite = new THREE.SpriteMaterial( { map: this.neutralTextureAsset.contents, depthTest: false } );
        this.shootingSprite1 = new THREE.SpriteMaterial( { map: this.fire1TextureAsset.contents, depthTest: false } );
        this.shootingSprite2 = new THREE.SpriteMaterial( { map: this.fire2TextureAsset.contents, depthTest: false } );
        this.altShootingSprite1 = new THREE.SpriteMaterial( { map: this.fireAlt1TextureAsset.contents, depthTest: false } );
        this.altShootingSprite2 = new THREE.SpriteMaterial( { map: this.fireAlt2TextureAsset.contents, depthTest: false } );

        this.sprite = new THREE.Sprite( this.neutralSprite );
        this.currentSprite = this.neutralSprite;
        this.sprite.scale.set(3, 3, 3);

        this.pivot = new THREE.Group();
        GAME.Globals.scene.add( this.pivot );

        this.pivot.add( this.sprite );                    
        this.sprite.position.set( this._restingPosition.x, this._restingPosition.y, this._restingPosition.z );
        this.origin = new THREE.Vector3();

        if (GAME.Globals.debug) {
            let geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2 );
            let material = new THREE.MeshStandardMaterial({emissive: 0x0000FF});
            this.originObject = new THREE.Mesh( geometry, material );
            GAME.Globals.scene.add( this.originObject );
        }

        this.resetOrigin();

        this.sound = new THREE.Audio( GAME.getListener() );
        this._audioLoader = new THREE.AudioLoader();
              
    }

    repositionToView(position, direction) {
        this.pivot.position.x = position.x;
        this.pivot.position.y = position.y;
        this.pivot.position.z = position.z;
        this.pivot.quaternion.set(direction.x, direction.y, direction.z, direction.w);
    }

    resetOrigin() {
        let baseSpritePosition = new THREE.Vector3();
        this.sprite.getWorldPosition(baseSpritePosition);
        this.origin.copy( baseSpritePosition );
        if (GAME.Globals.debug) {
            this.originObject.position.copy(this.origin);
            this.originObject.quaternion.copy(this.sprite.quaternion);
        }
    }

    changeSpriteTo( sprite ) {
        this.currentSprite = sprite;
        this.sprite.material = this.currentSprite;
        this.sprite.material.needsUpdate = true;
    }

    startShooting () {
        this.isShooting = true;
        let interval = 1000 / this.rateOfFire;
        //shoot once no matter what
        this.shoot();
        clearInterval(this._shootInterval);
        this._shootInterval = setInterval(() => {
            if (this.isShooting) {
                this.shoot();
            } 
        }, interval);
    }

    stopShooting () {
        this.isShooting = false;
        this.changeSpriteTo( this.neutralSprite );
        this.sprite.position.set( this._restingPosition.x, this._restingPosition.y, this._restingPosition.z );
        clearInterval(this._shootInterval);
    }

    shoot () {
        //this is a janky bad idea
        //it's not tied to the animation rate etc etc
        let animationSpeed = 32;
        let animation = [];
        animation.push((Math.random() > 0.2 ? this.shootingSprite1 : this.altShootingSprite1));
        animation.push((Math.random() > 0.2 ? this.shootingSprite2 : this.altShootingSprite2));
        animation.push(this.neutralSprite);
        
        this.changeSpriteTo( animation[0] );
        let animationIndex = 1;
        clearTimeout(this._animateFrameTimeout);
        this._animateFrameTimeout = setInterval(() => {
            if (animationIndex < animation.length) {
                this.changeSpriteTo( animation[animationIndex] );
                this.sprite.position.set( 
                    //randomly shake the gun around
                    this._restingPosition.x + HELPER.getRandom(-0.1, 0.1),
                    this._restingPosition.y + HELPER.getRandom(-0.1, 0.1),
                    this._restingPosition.z + HELPER.getRandom(-0.1, 0.1) );
            } else {
                clearTimeout(this._animateFrameTimeout);
            }
            animationIndex++;
        }, animationSpeed);

        this.resetOrigin();
        let bullet = new Bullet(this.origin, this.pivot.quaternion);
        bullet.initialize();

        let soundindex = HELPER.getRandomInt(0, 7);
        if (this.sound.isPlaying) {
            this.sound.stop();
        }
        this.sound.setBuffer( this._mgsounds[soundindex].contents );
        this.sound.setLoop( false );
        this.sound.setVolume( 0.5 );
        this.sound.play();
    }

};

export { Tommygun2 };