import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { Beacon } from './beacon.js';
import { RegionGate } from './regionGate.js';
import { Worm } from './worm.js';

class Region {

    uuid;
    isEndRegion;
    modifier;

    heightmapsrc;
    moonTextureAsset;
    heightmapData;
    heightData = [];

    group;
    object;
    width;
    height;
    minHeight;
    maxHeight;
    sizeAmplifier;
    heightModifier;

    entryPoint;
    exitPoint;

    constructor(heightmapsrc) {
        this.uuid = HELPER.getUUID();
        this.heightmapsrc = heightmapsrc;
        this.sizeAmplifier = 7;
        this.heightModifier = 0.5;
        this.moonTextureAsset = GAME.Globals.assets.find(x => x.name === 'moon');
        this.minHeight = 0;
        this.maxHeight = 100;
    }

    async prepareImageData() {
        return new Promise(function(resolve, reject) {
            let image = new Image();
            image.src = this.heightmapsrc;
            image.onload = () => {
                this.width = image.width;
                this.height = image.height;
        
                let canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                let context = canvas.getContext('2d');
        
                context.drawImage(image, 0, 0);
                this.heightmapData = context.getImageData(0, 0, this.width, this.height).data;
        
                resolve();
            }
        }.bind(this));
    }

    async initialize() {
        await this.initializeGraphics();
        await this.initializePhysics();
    }

    async initializeGraphics() {
        await this.prepareImageData();  
        let geometry = new THREE.PlaneBufferGeometry(this.width * this.sizeAmplifier, this.height * this.sizeAmplifier, this.width - 1, this.height - 1);   
        geometry.castShadow = true;
        geometry.receiveShadow = true;

        var vertices = geometry.attributes.position.array;
        //here we go over the hightmap data and buffer geometry verticies and apply the heightmap data to the buffer's Z verticies
        //why Z and not Y? Because the plane geometry is built along the X and Y axis by default. We're using Z for the height for now and then flipping the geometry later
        //why start at j = 2 and increase by +3 each iteration? Because it is a long array of verticies which comes in triplets (x,y,z), and we're only interested in the z values
        //heightmapData contains all the rbga values (one by one) for the heightmap in an array, so in the same fashion we jump forward 4 points each loop
        let children = [];
        let exitRegionDoor;
        for (var i = 0, j = 2, k = 0; i < this.heightmapData.length; i += 4, j += 3, k++) {
            //we use the GREEN channel for height, and the other channels for other data
            const r = this.heightmapData[i];
            const g = this.heightmapData[i+1];
            const b = this.heightmapData[i+2];
            
            vertices[j] = g * this.heightModifier;
            this.heightData[k] = vertices[j];
            if (r !== 0 || b !== 0) {
                //special pixel
                let location = new THREE.Vector3(vertices[j - 2], vertices[j], -vertices[j - 1]);
                if (b === 0 & r === 255) {
                    //beacon
                    let beacon = new Beacon();
                    beacon.initialize(location);
                    beacon.object.rotateZ(HELPER.getRandom(-0.1, 0.1));
                    beacon.object.rotateX(HELPER.getRandom(-0.1, 0.1));
                    children.push(beacon.object);
                } else if (b === 255 & r === 255) {
                    //exit
                    exitRegionDoor = new RegionGate();
                    exitRegionDoor.initialize(location);
                    exitRegionDoor.object.rotateY(Math.PI/2);
                    this.exitPoint = location;
                    children.push(exitRegionDoor.object);
                } else if (b === 255 & r === 0) {
                    //entrance
                    this.entryPoint = location;
                } else if (r === 100 & b === 0) {
                    //worm
                    location.y += 2; //spawn worm a little in the air so it won't be stuck
                    let worm = new Worm();
                    worm.initialize(location);
                    children.push(worm.object);
                }
            }
        }

        this.group = new THREE.Group();
        //here's where we flip the geometry to be rightways up
        geometry.rotateX(- Math.PI/2);
        let material = new THREE.MeshStandardMaterial({color: 0xbbbbbb, map: this.moonTextureAsset.contents});
        let floor = new THREE.Mesh(geometry, material);
        floor.name = "floor";
        GAME.Globals.solidObjects.push( floor );
        this.group.add(floor);

        for (var i = 0; i < children.length; i++) {
            this.group.add(children[i]);
        }
    
        GAME.Globals.scene.add( this.group );
        
        this.object = floor;
    }

    createAmmoTerrainShape() {
        // This parameter is not really used, since we are using PHY_FLOAT height data type and hence it is ignored
        const heightScale = 1;
        // Up axis = 0 for X, 1 for Y, 2 for Z. Normally 1 = Y is used.
        const upAxis = 1;
        // hdt, height data type. "PHY_FLOAT" is used. Possible values are "PHY_FLOAT", "PHY_UCHAR", "PHY_SHORT"
        const hdt = "PHY_FLOAT";
        // Set this to your needs (inverts the triangles)
        const flipQuadEdges = false;
        // Creates height data buffer in Ammo heap
        let ammoHeightData = Ammo._malloc( 4 * this.width * this.height );

        // Copy the javascript height data array to the Ammo one.
        let p = 0;
        let p2 = 0;
        for ( let j = 0; j < this.height; j ++ ) {
            for ( let i = 0; i < this.width; i ++ ) {
                // write 32-bit float data to memory
                Ammo.HEAPF32[ ammoHeightData + p2 >> 2 ] = this.heightData[p];
                p ++;
                // 4 bytes/float
                p2 += 4;
            }
        }

        // Creates the heightfield physics shape
        var heightFieldShape = new Ammo.btHeightfieldTerrainShape(
            this.width,
            this.height,
            ammoHeightData,
            heightScale,
            this.minHeight,
            this.maxHeight,
            upAxis,
            hdt,
            flipQuadEdges
        );

        // Set horizontal scale
        const terrainWidthExtents = this.width * this.sizeAmplifier;
        const terrainDepthExtents = this.height * this.sizeAmplifier;
        let scaleX = terrainWidthExtents / ( this.width - 1 );
        let scaleZ = terrainDepthExtents / ( this.height - 1 );
        heightFieldShape.setLocalScaling( new Ammo.btVector3( scaleX, 1, scaleZ ) );

        heightFieldShape.setMargin( 0.05 );

        return heightFieldShape;
    }

    async initializePhysics() {
        let groundShape = this.createAmmoTerrainShape();
        console.log(groundShape);
        let groundTransform = new Ammo.btTransform();
        groundTransform.setIdentity();
        // Shifts the terrain, since bullet re-centers it on its bounding box.
        groundTransform.setOrigin( new Ammo.btVector3( 0, ( this.maxHeight + this.minHeight ) / 2, 0 ) );
        let groundMass = 0;
        let groundLocalInertia = new Ammo.btVector3( 0, 0, 0 );
        let groundMotionState = new Ammo.btDefaultMotionState( groundTransform );
        let groundBody = new Ammo.btRigidBody( new Ammo.btRigidBodyConstructionInfo( groundMass, groundMotionState, groundShape, groundLocalInertia ) );
        GAME.Globals.Ammo.physicsWorld.addRigidBody( groundBody );

        GAME.Globals.Ammo._tmpTrans = new Ammo.btTransform();
    }

};

export { Region };