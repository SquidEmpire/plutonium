import * as THREE from './vendor/three.module.js';
import { Tommygun2 } from './tommygun2.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { GameObject } from './gameObject.js';

class Player extends GameObject {    
    // global
    controls;

    //internal
    object;
    _downRaycaster;
    _motionRaycaster;
    _interactionRaycaster;

    //state
    moveForward = false;
    moveBackward = false;
    moveLeft = false;
    moveRight = false;
    canJump = false;
    
    prevTime = performance.now();
    velocity = new THREE.Vector3();
    direction = new THREE.Vector3();    

    weapon;
    torch;

    //settings
    mass = 100;
    height = 1.5;
    jumpSpeed = 100;

    constructor( controls ) {

        super();

        this.controls = controls;        
        controls.getObject().position.set(0, 20, 0);

        document.addEventListener( 'keydown', this.onKeyDown.bind(this), false );
        document.addEventListener( 'keyup', this.onKeyUp.bind(this), false );       

        this._downRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3( 0, - 1, 0 ), 0, 10 );
        this._motionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 8 );
        this._interactionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 10 );

        let playerGeometry = new THREE.BoxGeometry( 4, 2, 4 );
        let playerMaterial = new THREE.MeshStandardMaterial({emissive: 0xffaa00});
    
        this.object = new THREE.Mesh( playerGeometry, playerMaterial );

        //controls change angle on mouse move which can happen many times per render so we need to reposition every time the controls change
        this.controls.addEventListener( 'change', this.repositionPlayer.bind(this) );

    }

    initialize() {
        this.torch = new THREE.SpotLight( 0xFFE6C9 );
        this.torch.castShadow = true;

        this.torch.shadow.mapSize.width = 512;
        this.torch.shadow.mapSize.height = 512;

        this.torch.castShadow = true;
        this.torch.shadow.mapSize.width = 1024;
        this.torch.shadow.mapSize.height = 1024;
        this.torch.shadow.camera.near = 1;
        this.torch.shadow.camera.far = 200;
        this.torch.shadow.radius = 1;

        this.torch.penumbra = 0.5;
        this.torch.decay = 2;
        this.torch.angle = 0.6;
        this.torch.distance = 200;

        this.object.add(this.torch);
        this.torch.position.set(0, 0, 1);
        this.object.add(this.torch.target);
        this.torch.target.position.set(0, -0.05, -2);

        if (GAME.Globals.debug) {
            let lightHelper = new THREE.SpotLightHelper( this.torch );
            GAME.Globals.scene.add( lightHelper );
        }

        this.object.position.y = 4;
        GAME.Globals.scene.add( this.object );
        super.initialize();
    }

    async arm() {
        //give us a tommygun
        this.weapon = new Tommygun2();
        this.weapon.initialize();
    }

    interact() {
        //check there's something in front of us we can interact with
        this._interactionRaycaster.setFromCamera( new THREE.Vector2(), this.controls.getObject() );
        let interactionIntersections = this._interactionRaycaster.intersectObjects( GAME.Globals.interactableObjects );

        if (interactionIntersections.length && interactionIntersections[0].object.userData.onInteract) {
            interactionIntersections[0].object.userData.onInteract();
        }        
    }

    onKeyDown ( event ) {

        switch ( event.keyCode ) {

            case 38: // up
            case 87: // w
                this.moveForward = true;
                break;

            case 37: // left
            case 65: // a
                this.moveLeft = true;
                break;

            case 40: // down
            case 83: // s
                this.moveBackward = true;
                break;

            case 39: // right
            case 68: // d
                this.moveRight = true;
                break;

            case 32: // space
                if ( this.canJump === true ) this.velocity.y += this.jumpSpeed;
                this.canJump = false;
                break;

            case 69: //e
                this.interact();

        }

    };

    onKeyUp ( event ) {

        switch ( event.keyCode ) {

            case 38: // up
            case 87: // w
                this.moveForward = false;
                break;

            case 37: // left
            case 65: // a
                this.moveLeft = false;
                break;

            case 40: // down
            case 83: // s
                this.moveBackward = false;
                break;

            case 39: // right
            case 68: // d
                this.moveRight = false;
                break;

        }
    }

    onMouseDown () {
        if (this.weapon) {
            this.weapon.startShooting();
        }
    }

    onMouseUp () {
        if (this.weapon) {
            this.weapon.stopShooting();
        }
    }

    onGameTickAlways () {
        var time = performance.now();

        if (HELPER.getRandomInt(0, 5) > 4 ) {
            this.torch.intensity = (1 - HELPER.getRandom(0, 0.2));
        }

        if ( this.controls.isLocked === true && GAME.Globals.paused === false ) {

            //first check if we're on a solid object
            this._downRaycaster.ray.origin.copy( this.controls.getObject().position );
            this._downRaycaster.ray.origin.y -= this.height;           
            
            this.object.updateMatrixWorld();
            let downwardIntersections = this._downRaycaster.intersectObjects( GAME.Globals.solidObjects );
            let onObject = downwardIntersections.length > 0;

            //now do some movement calculations to work out where we've asked to go
            let delta = ( time - this.prevTime ) / 1000;

            this.velocity.x -= this.velocity.x * 10.0 * delta;
            this.velocity.z -= this.velocity.z * 10.0 * delta;

            this.velocity.y -= GAME.Globals.gravity * this.mass * delta;

            this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
            this.direction.x = Number( this.moveRight ) - Number( this.moveLeft );
            this.direction.normalize(); // this ensures consistent movements in all directions          
            
            //set our velocity
            if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * 400.0 * delta;
            if ( this.moveLeft || this.moveRight ) this.velocity.x -= this.direction.x * 400.0 * delta;

            //before we move check the area is free
            this._motionRaycaster.ray.origin.copy( this.controls.getObject().position );
            this._motionRaycaster.ray.origin.y -= this.height;

            let raycastDirectionVector = new THREE.Vector3( this.direction.x, 0, 0 );
            raycastDirectionVector.applyQuaternion( this.controls.getObject().quaternion );
            this._motionRaycaster.ray.direction.copy(raycastDirectionVector);
            let movementXIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );

            raycastDirectionVector = new THREE.Vector3( 0, 0, -this.direction.z );
            raycastDirectionVector.applyQuaternion( this.controls.getObject().quaternion );
            this._motionRaycaster.ray.direction.copy(raycastDirectionVector);
            let movementZIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );

            if ( movementXIntersections.length > 0 ) this.velocity.x = 0;
            if ( movementZIntersections.length > 0 ) this.velocity.z = 0;

            if ( onObject === true ) {

                this.velocity.y = Math.max( 0, this.velocity.y );
                this.canJump = true;

            }

            //apply movement
            this.controls.moveRight( - this.velocity.x * delta );
            this.controls.moveForward( - this.velocity.z * delta );

            this.controls.getObject().position.y += ( this.velocity.y * delta );

            //stop us going below 0
            if ( this.controls.getObject().position.y < this.height ) {

                this.velocity.y = 0;
                this.controls.getObject().position.y = this.height;

                this.canJump = true;

            }

            //after calculating our position, reposition anything on us
            this.repositionPlayer();
        }

        this.prevTime = time;
    }

    repositionPlayer () {
        let position = new THREE.Vector3();
        let direction = new THREE.Quaternion();
        position.copy(this.controls.getObject().position);
        direction.copy(this.controls.getObject().quaternion);

        this.object.position.copy(position);
        this.object.quaternion.copy(direction);

        this.weapon.repositionToView(position, direction);
    }

};

export { Player };