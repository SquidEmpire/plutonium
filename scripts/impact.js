import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import * as IMPACTTYPE from './impactType.js';

class Impact {

    origin;
    type;
    sprite;
    pivot;
    uuid;
    animationDuration;
    loop;

    //animation internals
    _animation;
    _animationIndex;
    _frameDuration;
    _prevTime = performance.now();
    _timeSinceLastFrame = 0;

    //sprites
    currentSprite;
    impact1;
    impact2;
    impact3;
    impact4;
    impact5;
    impact6;

    constructor( origin, type ) {
        
        this.origin = origin;   
        this.animationDuration = 200; //milliseconds
        this.loop = false;
        this.type = type;
        
        this.impact1TextureAsset = GAME.Globals.assets.find(x => x.name === 'impact1');
        this.impact2TextureAsset = GAME.Globals.assets.find(x => x.name === 'impact2');
        this.impact3TextureAsset = GAME.Globals.assets.find(x => x.name === 'impact3');
        this.impact4TextureAsset = GAME.Globals.assets.find(x => x.name === 'impact4');
        this.impact5TextureAsset = GAME.Globals.assets.find(x => x.name === 'impact5');
        this.impact6TextureAsset = GAME.Globals.assets.find(x => x.name === 'impact6');

    }

    destroy() {
        GAME.Globals.animatedObjects.delete(this.uuid);
        GAME.Globals.scene.remove( this.pivot );
    }

    initialize() {

        this.uuid = HELPER.getUUID();

        this.impact1 = new THREE.SpriteMaterial( { map: this.impact1TextureAsset.contents } );
        this.impact2 = new THREE.SpriteMaterial( { map: this.impact2TextureAsset.contents } );
        this.impact3 = new THREE.SpriteMaterial( { map: this.impact3TextureAsset.contents } );
        this.impact4 = new THREE.SpriteMaterial( { map: this.impact4TextureAsset.contents } );
        this.impact5 = new THREE.SpriteMaterial( { map: this.impact5TextureAsset.contents } );
        this.impact6 = new THREE.SpriteMaterial( { map: this.impact6TextureAsset.contents } );

        if (this.type === IMPACTTYPE.fleshImpactType) {
            let bloodColour = new THREE.Color(0x42775D);
            this.impact1.color = bloodColour;
            this.impact2.color = bloodColour;
            this.impact3.color = bloodColour;
            this.impact4.color = bloodColour;
            this.impact5.color = bloodColour;
            this.impact6.color = bloodColour;
        }

        this.sprite = new THREE.Sprite( this.impact1 );
        this.currentSprite = this.impact1;
        this.sprite.scale.set(2, 10, 10);

        this.pivot = new THREE.Group();
        this.pivot.add( this.sprite );
        GAME.Globals.scene.add( this.pivot );
        this.pivot.position.set( 0, 1, 0 );

        this.pivot.position.copy( this.origin );

        this._animation = [this.impact1, this.impact2, this.impact3, this.impact4, this.impact5, this.impact6];
        this._animationIndex = 0;
        this._frameDuration = this.animationDuration / this._animation.length; 

        GAME.Globals.animatedObjects.set(this.uuid, this);
                
    }

    onAnimationEnd() {
        if (this.loop) {
            this._animationIndex = 0;
        } else {
            this.destroy();
        }
    }

    changeSpriteTo( sprite ) {
        this.currentSprite = sprite;
        this.sprite.material = this.currentSprite;
        this.sprite.material.needsUpdate = true;
    }

    onAnimate() {
        var time = performance.now();
        if (!GAME.Globals.paused) {
            var delta = ( time - this._prevTime );
            if (this._timeSinceLastFrame >= this._frameDuration) {
                if (this._animationIndex === this._animation.length) {
                    this.onAnimationEnd();
                } else {
                    this.changeSpriteTo( this._animation[this._animationIndex] );
                    this._animationIndex++;
                    this._timeSinceLastFrame = 0;
                }
            } else {
                this._timeSinceLastFrame += delta;
            }
        }
        this._prevTime = time;
    }

};

export { Impact };