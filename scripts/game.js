import * as THREE from './vendor/three.module.js';
import * as HELPER from './helper.js';

import { PointerLockControls } from './vendor/PointerLockControls.js';
import { Skybox } from './skybox.js';
import { Player } from './player.js';
import { Asset } from './asset.js';
import { Loader } from './loader.js';
import { Lighthouse } from './lighthouse.js';
import { Region } from './region.js';

var camera, renderer, controls;
var clock = new THREE.Clock();
var time = 0;
var timeNextSpawn = 1;

var listener, loader;

var Globals = {
    debug: false,
    gravity: 4, //9.8 = earth
    gameSpeed: 200, //ticks per second
    animatedObjects: new Map(),
    solidObjects: [], //in order to raycast on this three.js needs a normal array
    physicsObjects: [],
    interactableObjects: [],
    gameObjects: new Map(),
    scene: null,
    player: null,
    regions: new Map(),
    currentRegion: null,
    paused: false,
    assets: [],
    Ammo: {
        physicsWorld: null,        
        _tmpTrans: null,
    }
}

function buildAssetList() {

    //models

    Globals.assets.push(new Asset('lighthouse', 'models/lighthouse.json', THREE.Object3D));
    Globals.assets.push(new Asset('beacon', 'models/beacon.json', THREE.Object3D));
    Globals.assets.push(new Asset('regiongate', 'models/regiongate.json', THREE.Object3D));
    Globals.assets.push(new Asset('worm', 'models/worm.json', THREE.Object3D));

    //audio

    Globals.assets.push(new Asset('impact_1', './sounds/impact/sand_impact_bullet1.mp3', THREE.Audio));
    Globals.assets.push(new Asset('impact_2', './sounds/impact/sand_impact_bullet2.mp3', THREE.Audio));
    Globals.assets.push(new Asset('impact_3', './sounds/impact/sand_impact_bullet3.mp3', THREE.Audio));
    Globals.assets.push(new Asset('impact_4', './sounds/impact/sand_impact_bullet4.mp3', THREE.Audio));

    Globals.assets.push(new Asset('flesh_impact_1', './sounds/impact/flesh_impact_1.mp3', THREE.Audio));
    Globals.assets.push(new Asset('flesh_impact_2', './sounds/impact/flesh_impact_2.mp3', THREE.Audio));
    Globals.assets.push(new Asset('flesh_impact_3', './sounds/impact/flesh_impact_3.mp3', THREE.Audio));

    Globals.assets.push(new Asset('mg1', './sounds/mg/mg1.mp3', THREE.Audio));
    Globals.assets.push(new Asset('mg2', './sounds/mg/mg2.mp3', THREE.Audio));
    Globals.assets.push(new Asset('mg3', './sounds/mg/mg3.mp3', THREE.Audio));
    Globals.assets.push(new Asset('mg4', './sounds/mg/mg4.mp3', THREE.Audio));
    Globals.assets.push(new Asset('mg5', './sounds/mg/mg5.mp3', THREE.Audio));
    Globals.assets.push(new Asset('mg6', './sounds/mg/mg6.mp3', THREE.Audio));
    Globals.assets.push(new Asset('mg7', './sounds/mg/mg7.mp3', THREE.Audio));
    Globals.assets.push(new Asset('mg8', './sounds/mg/mg8.mp3', THREE.Audio));

    Globals.assets.push(new Asset('spider_death', './sounds/spider/spider_death.mp3', THREE.Audio));
    Globals.assets.push(new Asset('spider_sound_1', './sounds/spider/spider_sound_1.mp3', THREE.Audio));
    Globals.assets.push(new Asset('spider_sound_2', './sounds/spider/spider_sound_2.mp3', THREE.Audio));
    Globals.assets.push(new Asset('spider_sound_3', './sounds/spider/spider_sound_3.mp3', THREE.Audio));
    Globals.assets.push(new Asset('spider_sound_4', './sounds/spider/spider_sound_4.mp3', THREE.Audio));

    //textures

    Globals.assets.push(new Asset('skybox', [
        './sprites/skymap/px.png',
        "./sprites/skymap/nx.png",
        './sprites/skymap/py.png',
        "./sprites/skymap/ny.png",
        './sprites/skymap/pz.png',
        "./sprites/skymap/nz.png",
    ], THREE.CubeTexture));

    Globals.assets.push(new Asset('moon', './sprites/moon.png', THREE.Texture));

    Globals.assets.push(new Asset('tommy_fire_1', './sprites/tommy_fire_1.png', THREE.Texture));
    Globals.assets.push(new Asset('tommy_fire_2', './sprites/tommy_fire_2.png', THREE.Texture));
    Globals.assets.push(new Asset('tommy_fire_alt_1', './sprites/tommy_fire_alt_1.png', THREE.Texture));
    Globals.assets.push(new Asset('tommy_fire_alt_2', './sprites/tommy_fire_alt_2.png', THREE.Texture));
    Globals.assets.push(new Asset('tommy_fire_neutral', './sprites/tommy_neutral.png', THREE.Texture));

    Globals.assets.push(new Asset('impact1', './sprites/impact/impact1.png', THREE.Texture));
    Globals.assets.push(new Asset('impact2', './sprites/impact/impact2.png', THREE.Texture));
    Globals.assets.push(new Asset('impact3', './sprites/impact/impact3.png', THREE.Texture));
    Globals.assets.push(new Asset('impact4', './sprites/impact/impact4.png', THREE.Texture));
    Globals.assets.push(new Asset('impact5', './sprites/impact/impact5.png', THREE.Texture));
    Globals.assets.push(new Asset('impact6', './sprites/impact/impact6.png', THREE.Texture)); 

    Globals.assets.push(new Asset('spider_neutral_front', './sprites/spiders/spider_neutral_front.png', THREE.Texture));
    Globals.assets.push(new Asset('spider_step_forward1', './sprites/spiders/spider_step_forward_1.png', THREE.Texture));
    Globals.assets.push(new Asset('spider_step_forward2', './sprites/spiders/spider_step_forward_2.png', THREE.Texture));
    Globals.assets.push(new Asset('spider_step_forward3', './sprites/spiders/spider_step_forward_3.png', THREE.Texture));
    Globals.assets.push(new Asset('spider_jump_start_1', './sprites/spiders/spider_jump_start_1.png', THREE.Texture));
    Globals.assets.push(new Asset('spider_jump_start_2', './sprites/spiders/spider_jump_start_2.png', THREE.Texture));
    Globals.assets.push(new Asset('spider_jump', './sprites/spiders/spider_jump.png', THREE.Texture));
    Globals.assets.push(new Asset('spider_dead', './sprites/spiders/spider_dead.png', THREE.Texture));
}

function setupPhysicsWorld(){

    let collisionConfiguration  = new Ammo.btDefaultCollisionConfiguration();
    let dispatcher              = new Ammo.btCollisionDispatcher(collisionConfiguration);
    let overlappingPairCache    = new Ammo.btDbvtBroadphase();
    let solver                  = new Ammo.btSequentialImpulseConstraintSolver();

    Globals.Ammo.physicsWorld = new Ammo.btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
    Globals.Ammo.physicsWorld.setGravity(new Ammo.btVector3(0, /*Globals.gravity*/-10, 0));

}

async function init() {
    //setup Ammo
    await Ammo();
    setupPhysicsWorld();

    //build our asset list
    buildAssetList();

    //load
    let loaderTextElement = document.getElementById( 'loader-description' );
    loader = new Loader(loaderTextElement);    
    await loader.loadAssets(Globals.assets);

    //start the game ticks
    setInterval(tick.bind(this), 1000/Globals.gameSpeed);

    Globals.paused = false;

    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 2000 );

    Globals.scene = new THREE.Scene();
    Globals.scene.background = new THREE.Color( 0xffffff );
    Globals.scene.fog = new THREE.FogExp2( 0x000000, 0.01 );

    var light = new THREE.AmbientLight( 0x303D47 );
    Globals.scene.add( light );   
    
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    document.body.appendChild( renderer.domElement );
    
    initSkybox();    
    initPlayer();
    await initTestRegion();
    //await initStartRegion();
    initEnemies();

    Globals.paused = true;

    window.addEventListener( 'resize', onWindowResize, false );

    animate();

}

function initSkybox () {
    let skybox = new Skybox();
    skybox.initialize();
}

async function initTestRegion () {
    let region = new Region('./maps/test.png');
    await region.initialize();
    Globals.regions.set(region.uuid, region);
    Globals.currentRegion = region;
}

async function initStartRegion () {
    let region = new Region('./maps/start.png');
    await region.initialize();
    initLighthouse(new THREE.Vector3(-20, 76, 20), Math.PI/4);
    controls.getObject().position.y = 100; //move the player
    Globals.regions.set(region.uuid, region);
    Globals.currentRegion = region;
}

function initLighthouse (position, rotation) {
    let lighthouse = new Lighthouse();
    lighthouse.initialize(position);
    lighthouse.object.rotateY(rotation);
}

function initPlayer () {

    controls = new PointerLockControls( camera, document.body );

    let blocker = document.getElementById( 'blocker' );
    let instructions = document.getElementById( 'instructions' );
    let loader = document.getElementById( 'loader' );

    instructions.style.removeProperty('display');
    loader.style.display = 'none';

    instructions.addEventListener( 'click', (e) => {

        e.preventDefault();
        e.stopPropagation();
        controls.lock();

    }, false );

    controls.addEventListener( 'lock', () => {

        instructions.style.display = 'none';
        blocker.style.display = 'none';
        Globals.paused = false;

    } );

    controls.addEventListener( 'unlock', () => {

        blocker.style.display = 'block';
        instructions.style.display = '';
        Globals.paused = true;

    } );    

    Globals.scene.add( controls.getObject() ); 

    let player = new Player( controls );
    listener = new THREE.AudioListener();
    controls.getObject().add(listener);
    player.initialize();
    player.arm();
    Globals.player = player;   

    document.body.addEventListener( 'mousedown', () => {if (!Globals.paused) player.onMouseDown()} );
    document.body.addEventListener( 'mouseup', () => {if (!Globals.paused) player.onMouseUp()} );
}

function initEnemies() {
    //let spider1 = new Spider();
    //spider1.initialize(new THREE.Vector3(0, 3, -50));
    //let spider2 = new Spider();
    //spider2.initialize(new THREE.Vector3(-100, 3, -50));  
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

function updatePhysics( deltaTime ) {
    Globals.Ammo.physicsWorld.stepSimulation( deltaTime, 10 );
    // Update objects
    for ( var i = 0, il = Globals.physicsObjects.length; i < il; i ++ ) {
        let objThree = Globals.physicsObjects[ i ];
        let objPhys = objThree.userData.physicsBody;
        let ms = objPhys.getMotionState();
        if ( ms ) {
            ms.getWorldTransform( Globals.Ammo._tmpTrans );
            var p = Globals.Ammo._tmpTrans.getOrigin();
            var q = Globals.Ammo._tmpTrans.getRotation();
            objThree.position.set( p.x(), p.y(), p.z() );
            objThree.quaternion.set( q.x(), q.y(), q.z(), q.w() );
        }
    }
}

//called once every game tick
function tick() {
    let deltaTime = clock.getDelta();

    if ( Globals.physicsObjects.length < 100 && time > timeNextSpawn ) {

        generateObject();
        timeNextSpawn = time + 1;

    }

    if (!Globals.paused) {
        updatePhysics(deltaTime);
    }    

    Globals.gameObjects.forEach(object => {
        object.onGameTickAlways();
    });

    time += deltaTime;
}

function animate() {

    requestAnimationFrame( animate );

    Globals.animatedObjects.forEach(object => {
        object.onAnimate();
    });

    renderer.render( Globals.scene, camera );

}

function getListener() {
    return listener;
}

init();



















//debug
function generateObject() {

    var numTypes = 4;
    var objectType = Math.ceil( Math.random() * numTypes );

    var threeObject = null;
    var shape = null;

    var objectSize = 3;
    var margin = 0.05;

    switch ( objectType ) {

        case 1:
            // Sphere
            var radius = 1 + Math.random() * objectSize;
            threeObject = new THREE.Mesh( new THREE.SphereBufferGeometry( radius, 20, 20 ), createObjectMaterial() );
            shape = new Ammo.btSphereShape( radius );
            shape.setMargin( margin );
            break;
        case 2:
            // Box
            var sx = 1 + Math.random() * objectSize;
            var sy = 1 + Math.random() * objectSize;
            var sz = 1 + Math.random() * objectSize;
            threeObject = new THREE.Mesh( new THREE.BoxBufferGeometry( sx, sy, sz, 1, 1, 1 ), createObjectMaterial() );
            shape = new Ammo.btBoxShape( new Ammo.btVector3( sx * 0.5, sy * 0.5, sz * 0.5 ) );
            shape.setMargin( margin );
            break;
        case 3:
            // Cylinder
            var radius = 1 + Math.random() * objectSize;
            var height = 1 + Math.random() * objectSize;
            threeObject = new THREE.Mesh( new THREE.CylinderBufferGeometry( radius, radius, height, 20, 1 ), createObjectMaterial() );
            shape = new Ammo.btCylinderShape( new Ammo.btVector3( radius, height * 0.5, radius ) );
            shape.setMargin( margin );
            break;
        default:
            // Cone
            var radius = 1 + Math.random() * objectSize;
            var height = 2 + Math.random() * objectSize;
            threeObject = new THREE.Mesh( new THREE.ConeBufferGeometry( radius, height, 20, 2 ), createObjectMaterial() );
            shape = new Ammo.btConeShape( radius, height );
            break;

    }

    threeObject.position.set( 0, 20, 0 );    

    var mass = objectSize * 5;
    var localInertia = new Ammo.btVector3( 0, 0, 0 );
    shape.calculateLocalInertia( mass, localInertia );
    var transform = new Ammo.btTransform();
    transform.setIdentity();
    var pos = threeObject.position;
    transform.setOrigin( new Ammo.btVector3( pos.x, pos.y, pos.z ) );
    var motionState = new Ammo.btDefaultMotionState( transform );
    var rbInfo = new Ammo.btRigidBodyConstructionInfo( mass, motionState, shape, localInertia );
    var body = new Ammo.btRigidBody( rbInfo );

    threeObject.userData.physicsBody = body;

    threeObject.receiveShadow = true;
    threeObject.castShadow = true;

    Globals.scene.add( threeObject );
    Globals.physicsObjects.push( threeObject );
    Globals.Ammo.physicsWorld.addRigidBody( body );
    console.log('new shape');
}

function createObjectMaterial() {

    var c = Math.floor( Math.random() * ( 1 << 24 ) );
    return new THREE.MeshPhongMaterial( { color: c } );

}



















export { Globals, getListener };