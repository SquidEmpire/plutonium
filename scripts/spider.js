import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { GameObject } from './gameObject.js';
import * as IMPACTTYPE from './impactType.js';

class Spider extends GameObject {

    sprite;
    pivot;
    plane;
    direction;
    velocity;
    prevTime = performance.now();
    speed = 0.25;
    size = 12;
    health = 50;
    mass = 50;
    target;
    isDead;
    isStartingJump;
    isJumping;
    sound;

    //animation internals
    _animation;
    _animationIndex;
    _frameDuration;
    _prevTime = performance.now();
    _timeSinceLastFrame = 0;

    _neutralAnimation = [];
    _walkForwardAnimation= [];
    _jumpAnimation = [];
    _jumpFlyAnimation = []

    _directionDebugArrow;
    _downDebugArrow;
    _raycaster;
    _downRaycaster;

    _sounds;
    _deathSound;

    constructor() {

        super();

        this._animationIndex = 0;

        this.animationDuration = 600; //milliseconds

        this.asset_spider_neutral_front = GAME.Globals.assets.find(x => x.name === 'spider_neutral_front');
        this.asset_spider_step_forward1 = GAME.Globals.assets.find(x => x.name === 'spider_step_forward1'); 
        this.asset_spider_step_forward2 = GAME.Globals.assets.find(x => x.name === 'spider_step_forward2');
        this.asset_spider_step_forward3 = GAME.Globals.assets.find(x => x.name === 'spider_step_forward3');
        this.asset_spider_jump_start_1 = GAME.Globals.assets.find(x => x.name === 'spider_jump_start_1'); 
        this.asset_spider_jump_start_2 = GAME.Globals.assets.find(x => x.name === 'spider_jump_start_2');
        this.asset_spider_jump = GAME.Globals.assets.find(x => x.name === 'spider_jump');
        this.asset_spider_dead = GAME.Globals.assets.find(x => x.name === 'spider_dead');

        this.spider_neutral_front = new THREE.SpriteMaterial( { map: this.asset_spider_neutral_front.contents } );
        this.spider_step_forward1 = new THREE.SpriteMaterial( { map: this.asset_spider_step_forward1.contents } );
        this.spider_step_forward2 = new THREE.SpriteMaterial( { map: this.asset_spider_step_forward2.contents } );
        this.spider_step_forward3 = new THREE.SpriteMaterial( { map: this.asset_spider_step_forward3.contents } );
        this.spider_jump_start_1 = new THREE.SpriteMaterial( { map: this.asset_spider_jump_start_1.contents } );
        this.spider_jump_start_2 = new THREE.SpriteMaterial( { map: this.asset_spider_jump_start_2.contents } );
        this.spider_jump = new THREE.SpriteMaterial( { map: this.asset_spider_jump.contents } );
        this.spider_dead = new THREE.SpriteMaterial( { map: this.asset_spider_dead.contents } );

        this._neutralAnimation = [this.spider_neutral_front];
        this._walkForwardAnimation = [this.spider_neutral_front, this.spider_step_forward1, this.spider_step_forward2, this.spider_step_forward3];
        this._jumpAnimation = [this.spider_jump_start_1, this.spider_jump_start_2, this.spider_jump];
        this._jumpFlyAnimation = [this.spider_jump];

        let sound_1 = GAME.Globals.assets.find(x => x.name === 'spider_sound_1');
        let sound_2 = GAME.Globals.assets.find(x => x.name === 'spider_sound_2');
        let sound_3 = GAME.Globals.assets.find(x => x.name === 'spider_sound_3');
        let sound_4 = GAME.Globals.assets.find(x => x.name === 'spider_sound_4');
        this._deathSound = GAME.Globals.assets.find(x => x.name === 'spider_death');

        this._sounds = [sound_1, sound_2, sound_3, sound_4];

        this.sound = new THREE.PositionalAudio( GAME.getListener() );
 
    }

    async initialize( position ) {

        this.sprite = new THREE.Sprite( this.spider_neutral_front );
        this.currentSprite = this.spider_neutral_front;
        this.sprite.scale.set(this.size, this.size, this.size);
        this.sprite.name = "spider sprite";

        let planegeometry = new THREE.PlaneGeometry(this.size, this.size, this.size);
        let planematerial = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide, alphaTest: 0, visible: GAME.Globals.debug} );
        this.plane = new THREE.Mesh( planegeometry, planematerial );
        this.plane.userData.onShot = this.onShot.bind(this);
        this.plane.name = "spider plane"

        this._animation = this._walkForwardAnimation;        
        this._frameDuration = this.animationDuration / this._animation.length; 

        GAME.Globals.animatedObjects.set(this.uuid, this);
        GAME.Globals.solidObjects.push( this.plane );

        this.pivot = new THREE.Group();
        this.pivot.position.copy( position );
        this.pivot.add( this.sprite );
        this.pivot.add( this.plane );
        this.sprite.position.set( 0, 0, 0 );
        this.pivot.add(this.sound);

        this.target = new THREE.Vector3(0, 0, 0);
        this.direction = new THREE.Vector3(0, 0, 1);
        this.velocity = new THREE.Vector3();

        if (GAME.Globals.debug) {
            this._directionDebugArrow = new THREE.ArrowHelper( this.direction, this.pivot.position, 10, Math.random() * 0xffffff );
            GAME.Globals.scene.add(this._directionDebugArrow);

            this._downDebugArrow = new THREE.ArrowHelper( new THREE.Vector3( 0, -1, 0 ), this.pivot.position, 10, Math.random() * 0xffffff );
            GAME.Globals.scene.add(this._downDebugArrow);
        }

        this._raycaster = new THREE.Raycaster( new THREE.Vector3(), this.direction, 0, this.speed );
        this._downRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3( 0, -1, -0.5 ), 0, 10 );

        this.isDead = false;
              
        super.initialize();
    }

    

    die() {
        //make sure we're on the ground
        this._downRaycaster.ray.origin.copy( this.pivot.position );
        let downwardIntersection = this._downRaycaster.intersectObject( GAME.Globals.currentRegion.object );
        if (downwardIntersection.length) {
            this.pivot.position.y = downwardIntersection[0].point.y;
        } else if (this.pivot.position.y < 0) {
            //if we've fallen through the world float up
            this.pivot.position.y = 1;
        }
        this.changeSpriteTo( this.spider_dead );
        this._animation = [this.spider_dead];
        this._animationIndex = 0;
        this.isDead = true;
        this.onGameTick = () => {}; //performance
        this.onAnimate = () => {};

        this.pivot.remove( this.plane );
        const index = GAME.Globals.solidObjects.indexOf(this.plane);
        GAME.Globals.solidObjects.splice(index, 1);
        this.plane.geometry.dispose();
        this.plane.material.dispose();
        if (GAME.Globals.debug) {
            GAME.Globals.scene.remove( this._directionDebugArrow );
            GAME.Globals.scene.remove( this._downDebugArrow );
        }

        this.sound.setBuffer( this._deathSound.contents );
        this.sound.setVolume( 2 );
        this.sound.setRefDistance( 10 );
        if (this.sound.isPlaying) {
            this.sound.stop();
        }
        this.sound.play();
    }

    onShot() {
        console.log((HELPER.getRandomBool() ? 'oof' : 'ouch') + (HELPER.getRandomBool() ? ' owie' : ''), this.health);
        if (this.health > 1) {
            this.health--;
        } else {
            this.die();
        }
        return IMPACTTYPE.fleshImpactType;
    }

    jump() {
        //yeet
        this.isJumping = true;
        this.velocity.y = 70;
        this._animation = this._jumpFlyAnimation;
        this.isStartingJump = false;

        let soundindex = HELPER.getRandomInt(0, this._sounds.length - 1);
        this.sound.setBuffer( this._sounds[soundindex].contents );
        this.sound.setVolume( 2 );
        this.sound.setRefDistance( 10 );
        if (this.sound.isPlaying) {
            this.sound.stop();
        }
        this.sound.play();
    }

    setDirectionToTarget() {
        this.pivot.lookAt(this.target.x, 0, this.target.z);
        this.direction = new THREE.Vector3(0, 0, 1);
        this.direction.applyQuaternion( this.pivot.quaternion );
    }

    onGameTickAlways() {
        var time = performance.now();

        if (!GAME.Globals.paused && !this.isDead) {

            if (GAME.Globals.debug) {
                this._directionDebugArrow.position.copy( this.pivot.position );
                this._directionDebugArrow.setDirection( this.direction );
                this._downDebugArrow.position.copy( this.pivot.position );
            }

            this.target = GAME.Globals.player.object.position; //detect the player first?

            if (this.isJumping) {
                //jumping
                let delta = ( time - this.prevTime ) / 1000;
                this.pivot.position.y += ( this.velocity.y * delta );
                this.velocity.y -= GAME.Globals.gravity * this.mass * delta;

                this._raycaster.ray.origin.copy( this.pivot.position );
                this._raycaster.ray.direction.copy( this.direction );
                let intersects = this._raycaster.intersectObjects( GAME.Globals.solidObjects, true);
                let validIntersects = intersects.filter(e => e.object.name !== "spider plane");
                if (validIntersects.length === 0) {
                    this.pivot.translateZ( this.speed * 1.5 );
                }

                this._downRaycaster.ray.origin.copy( this.pivot.position );
                let downwardIntersection = this._downRaycaster.intersectObject( GAME.Globals.currentRegion.object );
                if (downwardIntersection.length && downwardIntersection[0].distance < 0.5) {
                    this.pivot.position.y = downwardIntersection[0].point.y;
                    this.isJumping = false;
                    this._animation = this._walkForwardAnimation;
                    this._animationIndex = 0;
                } else if (this.pivot.position.y < 0) {
                    //if we've fallen through the world float up
                    this.pivot.position.y = 1;
                    this.isJumping = false;
                    this._animation = this._walkForwardAnimation;
                    this._animationIndex = 0;
                }

            } else if (!this.isStartingJump && this.target !== null) {
                //walking
                this.setDirectionToTarget();
    
                this._raycaster.ray.origin.copy( this.pivot.position );
                this._raycaster.ray.direction.copy( this.direction );
    
                let intersects = this._raycaster.intersectObjects( GAME.Globals.solidObjects, true);
                if (intersects.length === 0) {
                    this.pivot.translateZ( this.speed );
                }
    
                this._downRaycaster.ray.origin.copy( this.pivot.position );
                let downwardIntersections = this._downRaycaster.intersectObject( GAME.Globals.currentRegion.object );
                if (downwardIntersections.length) {
                    //new position is higher so we need to move upwards
                    if (this.size/2 > downwardIntersections[0].distance) {
                        this.pivot.position.y += (this.size/2 - downwardIntersections[0].distance) - 1;
                    }                  
    
                    //prevent falling through floor
                    if (this.size/2 <= downwardIntersections[0].distance) {
                        this.pivot.position.y = downwardIntersections[0].point.y;
                    }
                } else {
                    if (this.pivot.position.y < 1) {
                        this.pivot.position.y = 1;
                    }
                }
                let worldPosition = new THREE.Vector3();
                this.pivot.getWorldPosition ( worldPosition )
                if (worldPosition.distanceTo(this.target) < 30) {
                    this.isStartingJump = true;
                    this._animation = this._jumpAnimation;
                    this._animationIndex = 0;
                }
            }  
        }
        this.prevTime = time;
    }

    onAnimationEnd() {
        if (this.isStartingJump) {
            this.jump();
        }
        this._animationIndex = 0;
    }

    changeSpriteTo( sprite ) {
        this.currentSprite = sprite;
        this.sprite.material = this.currentSprite;
        this.sprite.material.needsUpdate = true;
    }

    onAnimate() {
        var time = performance.now();
        if (!GAME.Globals.paused) {
            var delta = ( time - this._prevTime );
            if (this._timeSinceLastFrame >= this._frameDuration) {
                if (this._animationIndex === this._animation.length) {
                    this.onAnimationEnd();           
                } else {
                    this.changeSpriteTo( this._animation[this._animationIndex] );
                    this._animationIndex++;
                    this._timeSinceLastFrame = 0;
                }
            } else {
                this._timeSinceLastFrame += delta;
            }
        }
        this._prevTime = time;
    }   

};

export { Spider };