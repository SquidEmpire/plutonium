class Asset {

    name;
    path;
    type;
    contents;

    constructor( name, path, type ) {
        this.name = name;
        this.path = path; //string or array of strings
        this.type = type;
    }

    setContents(contents) {
        this.contents = contents;
    }

    clone() {
        let clone = new Asset(this.name, this.path, this.type);
        clone.setContents(this.contents.clone());
        return clone;
    }

};

export { Asset };