import * as THREE from './vendor/three.module.js';
import { SCENE } from './game.js';

class Tommygun {

    pivot;
    object;

    constructor(  ) {
 
    }

    async load() {
        return new Promise(function(resolve, reject) {
            var loader = new THREE.ObjectLoader();
            loader.load(
                "models/tommy.json",
                (object) => {
                    this.object = object;
                    this._setupObject();
                    resolve();
                },
                function ( xhr ) {
                    console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
                },
                function ( err ) {
                    console.error( 'An error happened' );
                    reject();
                }
            );
        }.bind(this));
    }

    _setupObject() {

        this.pivot = new THREE.Group();
        this.object.scale.set(0.5, 0.5, 0.5);
        SCENE.add( this.pivot );
        this.pivot.add( this.object );
        this.object.position.set( 1, -1.5, -5 );

    }

    repositionToView(position, direction) {
        this.pivot.position.x = position.x;
        this.pivot.position.y = position.y;
        this.pivot.position.z = position.z;
        
        this.pivot.quaternion.set(direction.x, direction.y, direction.z, direction.w);
    }

    getObject() {
        return this.object;
    }

};

export { Tommygun };